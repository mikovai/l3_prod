MERGE INTO dprepo.l3_prod l3  

USING (

SELECT 
rn,cif,subj_id,acc_num,age,gender_flag,
first_touch_point,fulfillment_cnl,promotional_code,sum_ibs_login,sum_mequa_login,first_month_bal,FIRST_3_MONTHS_BAL,lifetime_avgbal,
LIFETIME_ATM_AMT,LIFETIME_POS_AMT,
OWNR_CLOSE_FLAG,OWNR_OPEN_FLAG,OWNR_FROZEN_FLAG,
APPL_CL_FLAG,OWNR_CL_FLAG,OWNR_RODS_FLAG,APPL_SA_FLAG,OWNR_SA_FLAG,APPL_TD_FLAG,OWNR_TD_FLAG,APPL_MRG_FLAG,OWNR_MRG_FLAG,OWNR_INST_FLAG,
appl_date,act_date,close_date,act_subj_flag,
DIFF_APPL_ACT_DAYS,DIFF_APPL_ACT_MONTHS,DIFF_APPL_ACT_YEARS,
DIFF_APPL_CLOSE_DAYS,DIFF_APPL_CLOSE_MONTHS,DIFF_APPL_CLOSE_YEARS,
DIFF_ACT_CLOSE_DAYS,DIFF_ACT_CLOSE_MONTHS,DIFF_ACT_CLOSE_YEARS,
DIFF_APPL_SYS_DAYS,DIFF_APPL_SYS_MONTHS,DIFF_APPL_SYS_YEARS,
DIFF_ACT_SYS_DAYS,DIFF_ACT_SYS_MONTHS, DIFF_ACT_SYS_YEARS,

ACT_DATE_CL,DIFF_ACT_CL_DAYS,DIFF_ACT_CL_MONTHS,DIFF_ACT_CL_YEARS,
DIFF_ACT_SYS_CL_YEARS,DIFF_ACT_SYS_CL_MONTHS,DIFF_ACT_SYS_CL_DAYS,

ACT_DATE_RODS,DIFF_ACT_RODS_DAYS,DIFF_ACT_RODS_MONTHS,DIFF_ACT_RODS_YEARS,
DIFF_ACT_SYS_RODS_YEARS,DIFF_ACT_SYS_RODS_MONTHS,DIFF_ACT_SYS_RODS_DAYS,

ACT_DATE_SA,DIFF_ACT_SA_DAYS,DIFF_ACT_SA_MONTHS,DIFF_ACT_SA_YEARS,
DIFF_ACT_SYS_SA_YEARS,DIFF_ACT_SYS_SA_MONTHS,DIFF_ACT_SYS_SA_DAYS,

ACT_DATE_TD,DIFF_ACT_TD_DAYS,DIFF_ACT_TD_MONTHS,DIFF_ACT_TD_YEARS,
DIFF_ACT_SYS_TD_YEARS,DIFF_ACT_SYS_TD_MONTHS,DIFF_ACT_SYS_TD_DAYS,

ACT_DATE_MRG,DIFF_ACT_MRG_DAYS,DIFF_ACT_MRG_MONTHS,DIFF_ACT_MRG_YEARS,
DIFF_ACT_SYS_MRG_YEARS,DIFF_ACT_SYS_MRG_MONTHS,DIFF_ACT_SYS_MRG_DAYS,

ACT_DATE_INST,DIFF_ACT_INST_DAYS,DIFF_ACT_INST_MONTHS,DIFF_ACT_INST_YEARS,
DIFF_ACT_SYS_INST_YEARS,DIFF_ACT_SYS_INST_MONTHS,DIFF_ACT_SYS_INST_DAYS,

MIN_D,MIN_M,MIN_Y,NAME_D,NAME_M,NAME_Y,
PRE_APPR_OFR_FLAG,
MRKT_CNST_FLAG

FROM (
SELECT 
A.*,
CASE WHEN A.MIN_D IN A.DIFF_ACT_CL_DAYS THEN 'CL' 
     WHEN A.MIN_D IN A.DIFF_ACT_RODS_DAYS THEN 'RODS'
     WHEN A.MIN_D IN A.DIFF_ACT_SA_DAYS THEN 'SA'  
     WHEN A.MIN_D IN A.DIFF_ACT_TD_DAYS THEN 'TD'
     WHEN A.MIN_D IN A.DIFF_ACT_MRG_DAYS THEN 'MRG'
     WHEN A.MIN_D IN A.DIFF_ACT_INST_DAYS THEN 'INST' ELSE NULL END NAME_D,
CASE WHEN A.MIN_M IN A.DIFF_ACT_CL_MONTHS THEN 'CL' 
     WHEN A.MIN_M IN A.DIFF_ACT_RODS_MONTHS THEN 'RODS'
     WHEN A.MIN_M IN A.DIFF_ACT_SA_MONTHS THEN 'SA'  
     WHEN A.MIN_M IN A.DIFF_ACT_TD_MONTHS THEN 'TD'
     WHEN A.MIN_M IN A.DIFF_ACT_MRG_MONTHS THEN 'MRG'
     WHEN A.MIN_M IN A.DIFF_ACT_INST_MONTHS THEN 'INST' ELSE NULL END NAME_M,
CASE WHEN A.MIN_Y IN A.DIFF_ACT_CL_YEARS THEN 'CL' 
     WHEN A.MIN_Y IN A.DIFF_ACT_RODS_YEARS THEN 'RODS'
     WHEN A.MIN_Y IN A.DIFF_ACT_SA_YEARS THEN 'SA'  
     WHEN A.MIN_Y IN A.DIFF_ACT_TD_YEARS THEN 'TD'
     WHEN A.MIN_Y IN A.DIFF_ACT_MRG_YEARS THEN 'MRG'
     WHEN A.MIN_Y IN A.DIFF_ACT_INST_YEARS THEN 'INST' ELSE NULL END NAME_Y     
FROM(  
SELECT
A.*,
CASE WHEN MIN_D_1=99999 THEN NULL ELSE MIN_D_1 END MIN_D,
CASE WHEN MIN_M_1=99999 THEN NULL ELSE MIN_M_1 END MIN_M,
CASE WHEN MIN_Y_1=99999 THEN NULL ELSE MIN_Y_1 END MIN_Y
FROM(
select A.*,
least(coalesce(DIFF_ACT_CL_DAYS,99999),
      coalesce(DIFF_ACT_RODS_DAYS,99999),
      coalesce(DIFF_ACT_SA_DAYS,99999),
      coalesce(DIFF_ACT_TD_DAYS,99999),
      coalesce(DIFF_ACT_MRG_DAYS,99999),
      coalesce(DIFF_ACT_INST_DAYS,99999)) AS MIN_D_1,
least(coalesce(DIFF_ACT_CL_MONTHS,99999),
      coalesce(DIFF_ACT_RODS_MONTHS,99999),
      coalesce(DIFF_ACT_SA_MONTHS,99999),
      coalesce(DIFF_ACT_TD_MONTHS,99999),
      coalesce(DIFF_ACT_MRG_MONTHS,99999),
      coalesce(DIFF_ACT_INST_MONTHS,99999)) AS MIN_M_1,
least(coalesce(DIFF_ACT_CL_YEARS,99999),
      coalesce(DIFF_ACT_RODS_YEARS,99999),
      coalesce(DIFF_ACT_SA_YEARS,99999),
      coalesce(DIFF_ACT_TD_YEARS,99999),
      coalesce(DIFF_ACT_MRG_YEARS,99999),
      coalesce(DIFF_ACT_INST_YEARS,99999)) AS MIN_Y_1      
      from  
      (select 
rn,cif,subj_id,acc_num,age,gender_flag,
first_touch_point,fulfillment_cnl,promotional_code,sum_ibs_login,sum_mequa_login,first_month_bal,FIRST_3_MONTHS_BAL,lifetime_avgbal,
LIFETIME_ATM_AMT,LIFETIME_POS_AMT,
OWNR_CLOSE_FLAG,OWNR_OPEN_FLAG,OWNR_FROZEN_FLAG,
APPL_CL_FLAG,OWNR_CL_FLAG,OWNR_RODS_FLAG,APPL_SA_FLAG,OWNR_SA_FLAG,APPL_TD_FLAG,OWNR_TD_FLAG,APPL_MRG_FLAG,OWNR_MRG_FLAG,OWNR_INST_FLAG,
appl_date,act_date,close_date,act_subj_flag,
DIFF_APPL_ACT_DAYS,DIFF_APPL_ACT_MONTHS,DIFF_APPL_ACT_YEARS,
DIFF_APPL_CLOSE_DAYS,DIFF_APPL_CLOSE_MONTHS,DIFF_APPL_CLOSE_YEARS,
DIFF_ACT_CLOSE_DAYS,DIFF_ACT_CLOSE_MONTHS,DIFF_ACT_CLOSE_YEARS,
DIFF_APPL_SYS_DAYS,DIFF_APPL_SYS_MONTHS,DIFF_APPL_SYS_YEARS,
DIFF_ACT_SYS_DAYS,DIFF_ACT_SYS_MONTHS, DIFF_ACT_SYS_YEARS,

ACT_DATE_CL,DIFF_ACT_CL_DAYS,DIFF_ACT_CL_MONTHS,DIFF_ACT_CL_YEARS,
DIFF_ACT_SYS_CL_YEARS,DIFF_ACT_SYS_CL_MONTHS,DIFF_ACT_SYS_CL_DAYS,

ACT_DATE_RODS,DIFF_ACT_RODS_DAYS,DIFF_ACT_RODS_MONTHS,DIFF_ACT_RODS_YEARS,
DIFF_ACT_SYS_RODS_YEARS,DIFF_ACT_SYS_RODS_MONTHS,DIFF_ACT_SYS_RODS_DAYS,

ACT_DATE_SA,DIFF_ACT_SA_DAYS,DIFF_ACT_SA_MONTHS,DIFF_ACT_SA_YEARS,
DIFF_ACT_SYS_SA_YEARS,DIFF_ACT_SYS_SA_MONTHS,DIFF_ACT_SYS_SA_DAYS,

ACT_DATE_TD,DIFF_ACT_TD_DAYS,DIFF_ACT_TD_MONTHS,DIFF_ACT_TD_YEARS,
DIFF_ACT_SYS_TD_YEARS,DIFF_ACT_SYS_TD_MONTHS,DIFF_ACT_SYS_TD_DAYS,

ACT_DATE_MRG,DIFF_ACT_MRG_DAYS,DIFF_ACT_MRG_MONTHS,DIFF_ACT_MRG_YEARS,
DIFF_ACT_SYS_MRG_YEARS,DIFF_ACT_SYS_MRG_MONTHS,DIFF_ACT_SYS_MRG_DAYS,

ACT_DATE_INST,DIFF_ACT_INST_DAYS,DIFF_ACT_INST_MONTHS,DIFF_ACT_INST_YEARS,
DIFF_ACT_SYS_INST_YEARS,DIFF_ACT_SYS_INST_MONTHS,DIFF_ACT_SYS_INST_DAYS,

PRE_APPR_OFR_FLAG,
MRKT_CNST_FLAG
from (select 
ROW_Number () over (partition by c.subj_id order by c.subj_id) as RN,
c.*,
case when j.OWNR_RODS_FLAG is null then 0 else j.OWNR_RODS_FLAG end as OWNR_RODS_FLAG,j.act_date_RODS,o.Lifetime_AvgBal,
TRUNC(months_between(j.act_date_RODS,c.ACT_DATE) /12) AS DIFF_ACT_RODS_YEARS,
TRUNC(months_between(j.act_date_RODS,c.ACT_DATE)) AS DIFF_ACT_RODS_MONTHS,
to_date(j.act_date_RODS) - to_date(c.ACT_DATE) AS DIFF_ACT_RODS_DAYS,

TRUNC(months_between(sysdate-1,j.act_date_RODS) /12) AS DIFF_ACT_SYS_RODS_YEARS,
TRUNC(months_between(sysdate-1,j.act_date_RODS)) AS DIFF_ACT_SYS_RODS_MONTHS,
to_date(sysdate-1) - to_date(j.act_date_RODS) AS DIFF_ACT_SYS_RODS_DAYS,

case when k.OWNR_CL_FLAG is null then 0 else k.OWNR_CL_FLAG end as OWNR_CL_FLAG,k.act_date_cl,
TRUNC(months_between(k.act_date_cl,c.ACT_DATE) /12) AS DIFF_ACT_CL_YEARS,
TRUNC(months_between(k.act_date_cl,c.ACT_DATE)) AS DIFF_ACT_CL_MONTHS,
to_date(k.act_date_cl) - to_date(c.ACT_DATE) AS DIFF_ACT_CL_DAYS,

TRUNC(months_between(sysdate-1,k.act_date_cl) /12) AS DIFF_ACT_SYS_CL_YEARS,
TRUNC(months_between(sysdate-1,k.act_date_cl)) AS DIFF_ACT_SYS_CL_MONTHS,
to_date(sysdate-1) - to_date(k.act_date_cl) AS DIFF_ACT_SYS_CL_DAYS

from (
select
b.*,case when i.OWNR_MRG_FLAG is null then 0 else i.OWNR_MRG_FLAG end as OWNR_MRG_FLAG,i.act_date_MRG,
TRUNC(months_between(i.act_date_MRG,b.ACT_DATE) /12) AS DIFF_ACT_MRG_YEARS,
TRUNC(months_between(i.act_date_MRG,b.ACT_DATE)) AS DIFF_ACT_MRG_MONTHS,
to_date(i.act_date_MRG) - to_date(b.ACT_DATE) AS DIFF_ACT_MRG_DAYS,

TRUNC(months_between(sysdate-1,i.act_date_MRG) /12) AS DIFF_ACT_SYS_MRG_YEARS,
TRUNC(months_between(sysdate-1,i.act_date_MRG)) AS DIFF_ACT_SYS_MRG_MONTHS,
to_date(sysdate-1) - to_date(i.act_date_MRG) AS DIFF_ACT_SYS_MRG_DAYS

from(
select a.*,
case when f.OWNR_INST_FLAG is null then 0 else f.OWNR_INST_FLAG end as OWNR_INST_FLAG,f.act_date_INST,
TRUNC(months_between(f.act_date_INST,a.ACT_DATE) /12) AS DIFF_ACT_INST_YEARS,
TRUNC(months_between(f.act_date_INST,a.ACT_DATE)) AS DIFF_ACT_INST_MONTHS,
to_date(f.act_date_INST) - to_date(a.ACT_DATE) AS DIFF_ACT_INST_DAYS,

TRUNC(months_between(sysdate-1,f.act_date_INST) /12) AS DIFF_ACT_SYS_INST_YEARS,
TRUNC(months_between(sysdate-1,f.act_date_INST)) AS DIFF_ACT_SYS_INST_MONTHS,
to_date(sysdate-1) - to_date(f.act_date_INST) AS DIFF_ACT_SYS_INST_DAYS,

case when g.OWNR_SA_FLAG is null then 0 else g.OWNR_SA_FLAG end as OWNR_SA_FLAG,g.act_date_SA,
TRUNC(months_between(g.act_date_SA,a.ACT_DATE) /12) AS DIFF_ACT_SA_YEARS,
TRUNC(months_between(g.act_date_SA,a.ACT_DATE)) AS DIFF_ACT_SA_MONTHS,
to_date(g.act_date_SA) - to_date(a.ACT_DATE) AS DIFF_ACT_SA_DAYS,

TRUNC(months_between(sysdate-1,g.act_date_SA) /12) AS DIFF_ACT_SYS_SA_YEARS,
TRUNC(months_between(sysdate-1,g.act_date_SA)) AS DIFF_ACT_SYS_SA_MONTHS,
to_date(sysdate-1) - to_date(g.act_date_SA) AS DIFF_ACT_SYS_SA_DAYS,

case when h.OWNR_TD_FLAG is null then 0 else h.OWNR_TD_FLAG end as OWNR_TD_FLAG,h.act_date_TD,
TRUNC(months_between(h.act_date_TD,a.ACT_DATE) /12) AS DIFF_ACT_TD_YEARS,
TRUNC(months_between(h.act_date_TD,a.ACT_DATE)) AS DIFF_ACT_TD_MONTHS,
to_date(h.act_date_TD) - to_date(a.ACT_DATE) AS DIFF_ACT_TD_DAYS,

TRUNC(months_between(sysdate-1,h.act_date_TD) /12) AS DIFF_ACT_SYS_TD_YEARS,
TRUNC(months_between(sysdate-1,h.act_date_TD)) AS DIFF_ACT_SYS_TD_MONTHS,
to_date(sysdate-1) - to_date(h.act_date_TD) AS DIFF_ACT_SYS_TD_DAYS

from (
select b.*,c.sum_ibs_login,d.sum_mequa_login,e.FIRST_MONTH_BAL,e.FIRST_3_MONTHS_BAL,
TRUNC(months_between(CLOSE_DATE,ACT_DATE) /12) AS DIFF_ACT_CLOSE_YEARS,
TRUNC(months_between(CLOSE_DATE,ACT_DATE)) AS DIFF_ACT_CLOSE_MONTHS,
to_date(CLOSE_DATE) - to_date(ACT_DATE) AS DIFF_ACT_CLOSE_DAYS,
TRUNC(months_between(CLOSE_DATE,APPL_DATE) /12) AS DIFF_APPL_CLOSE_YEARS,
TRUNC(months_between(CLOSE_DATE,APPL_DATE)) AS DIFF_APPL_CLOSE_MONTHS,
to_date(CLOSE_DATE) - to_date(APPL_DATE) AS DIFF_APPL_CLOSE_DAYS,
TRUNC(months_between(ACT_DATE,APPL_DATE) /12) AS DIFF_APPL_ACT_YEARS,
TRUNC(months_between(ACT_DATE,APPL_DATE)) AS DIFF_APPL_ACT_MONTHS,
to_date(ACT_DATE) - to_date(APPL_DATE) AS DIFF_APPL_ACT_DAYS,
TRUNC(months_between(sysdate-1,APPL_DATE) /12) AS DIFF_APPL_SYS_YEARS,
TRUNC(months_between(sysdate-1,APPL_DATE)) AS DIFF_APPL_SYS_MONTHS,
to_date(sysdate-1) - to_date(APPL_DATE) AS DIFF_APPL_SYS_DAYS,
TRUNC(months_between(sysdate-1,ACT_DATE) /12) AS DIFF_ACT_SYS_YEARS,
TRUNC(months_between(sysdate-1,ACT_DATE)) AS DIFF_ACT_SYS_MONTHS,
to_date(sysdate-1) - to_date(ACT_DATE) AS DIFF_ACT_SYS_DAYS

from
(select 
a.*,
case when APPL_DATE is not null and  ACT_DATE is not null and CLOSE_DATE is not null  then '1' else '0' end as OWNR_CLOSE_FLAG,
case when APPL_DATE is not null and  ACT_DATE is not null and CLOSE_DATE is null      then '1' else '0' end as OWNR_OPEN_FLAG,
case when (APPL_DATE is not null and  ACT_DATE is null    and CLOSE_DATE is not null) or (APPL_DATE is not null and  ACT_DATE is null and CLOSE_DATE is null) then '1' else '0' end as OWNR_FROZEN_FLAG
from 
(select 
CIF,SUBJ_ID,ACC_NUM,AGE,
case when GENDER='Žena' then '1' else '0' end as GENDER_FLAG,
case when APPL_DATE > ACT_DATE then ACT_DATE  else APPL_DATE end as APPL_DATE ,
ACT_DATE,
CLOSE_DATE,
ACT_SUBJ_FLAG,
MRKT_CNST_FLAG,
PRE_APPR_OFR_FLAG,
MORT_APPLICATION_C as APPL_MRG_FLAG,
CL_APPLICATION_C as APPL_CL_FLAG,
TD_APPLICATION_C as APPL_TD_FLAG,
SA_APPLICATION_C as APPL_SA_FLAG,
LIFETIME_ATM_AMT, 
LIFETIME_POS_AMT,
case 
when SUBSTR(first_touch_point_id,-8,2)=17 then 1                  /* Call Centrum */               
when SUBSTR(first_touch_point_id,-8,2) in (14,30) then 2          /* Externi sit */
when SUBSTR(first_touch_point_id,-8,2)=15 then 3                  /* Internetove bankovnictvi */
when SUBSTR(first_touch_point_id,-8,2)=18 then 4                  /* Online */
when SUBSTR(first_touch_point_id,-8,2) in (10,11,12) then 5       /* Pobocky */
else 6 end as FIRST_TOUCH_POINT,                                  /* Other */
case 
when SUBSTR(FULFILLMENT_CNL_ID,-8,2)=17 then 1                   /* Call Centrum */
when SUBSTR(FULFILLMENT_CNL_ID,-8,2) in (14,30) then 2           /* Externi sit */
when SUBSTR(FULFILLMENT_CNL_ID,-8,2)=15 then 3                   /* Internetove bankovnictvi */
when SUBSTR(FULFILLMENT_CNL_ID,-8,2)=18 then 4                   /* Online */
when SUBSTR(FULFILLMENT_CNL_ID,-8,2) in (10,11,12) then 5        /* Pobocky */
else 6 end as FULFILLMENT_CNL,                                   /* Other */
case when PROMOTIONAL_CODE is not null then 1 else 0 end as PROMOTIONAL_CODE 
from L3_EKG_MIKRO)a)b
left join (select cif,sum(case when count_ibs_login>0 then count_ibs_login else 0 end) as sum_ibs_login
           from l3_ibs_login where upd_dt>to_date(sysdate-3) group by CIF order by cif)c on c.cif=b.cif
left join (select cif,sum(case when count_mequa_login>0 then count_mequa_login else 0 end) as sum_mequa_login
           from l3_mequa_login where upd_dt>to_date(sysdate-3) group by CIF order by cif)d on d.cif=b.cif
left join (select acc_num,FIRST_MONTH_BAL, FIRST_3_MONTHS_BAL from l3_accounts where acc_class_descr='Běžný účet')e on e.acc_num=b.acc_num)a
left join (select b.subj_id,a.subj_id as subj,a.act_date,b.act_date_inst,b.rn, case when a.act_date is not null then 1 else 0 end as OWNR_INST_FLAG from  l3_ekg_mikro a     
           left join (select b.subj_id,a.sign_date as act_date_INST,a.owner_uni_pt_key,b.uni_pt_key,ROW_Number () over (partition by b.subj_id order by b.subj_id) as RN  from adsl2_risk_owner.product a        
           left join adsl2_crm_owner.cmt_subj b on b.uni_pt_key = a.owner_uni_pt_key where b.uni_pt_key is not null and a.acc_class_id='JUPITER' and a.contr_num is not null and current_flag=1 and a.upd_dt>to_date(sysdate-3))b on b.subj_id=a.subj_id
           where b.subj_id is not null and b.act_date_inst >= a.act_date and RN=1)f on f.subj_id=a.subj_id
left join (select subj_id,act_date_sa, case when act_date_sa is not null then 1 else 0 end as OWNR_SA_FLAG from
          (select b.*, a.subj_id ,a.act_date,ROW_Number () over (partition by subj_id,act_date order by subj_id,act_date) as RN from l3_ekg_mikro a
           left join (select subj_id as subj_sa,act_date as act_date_sa from adsl2_crm_owner.cmt_depo_appl where acc_class_id like 'SA%' and upd_dt>to_date(sysdate-3))b on b.subj_sa=a.subj_id)
           where act_date_sa >= act_date and RN=1)g on g.subj_id=a.subj_id
left join (select subj_id,act_date_td, case when act_date_td is not null then 1 else 0 end as OWNR_TD_FLAG from
          (select b.*, a.subj_id ,a.act_date,ROW_Number () over (partition by subj_id,act_date order by subj_id,act_date) as RN from l3_ekg_mikro a
           left join (select subj_id as subj_td,act_date as act_date_td from adsl2_crm_owner.cmt_depo_appl where acc_class_id like 'TD%' and upd_dt>to_date(sysdate-3))b on b.subj_td=a.subj_id)
           where act_date_td >= act_date and RN=1)h on h.subj_id=a.subj_id)b
left join (select subj_id,act_date_mrg, case when act_date_mrg is not null then 1 else 0 end as OWNR_MRG_FLAG from
          (select b.*, a.subj_id ,a.act_date,ROW_Number () over (partition by subj_id,act_date order by subj_id,act_date) as RN from l3_ekg_mikro a
           left join (select subj_id as subj_mrg,to_date(DDN_START_DATE,'DD.MM.YY') as act_date_MRG from adsl2_crm_owner.cmt_mort_appl where upd_dt>to_date(sysdate-3))b on b.subj_mrg=a.subj_id)
           where act_date_MRG >= act_date and RN=1)i on i.subj_id=b.subj_id)c           
left join (select a.* from (select subj_id,act_date_rods,OWNR_RODS_FLAG from(
           select a.*,b.*, ROW_Number () over (partition by subj,act_date_ca order by subj,act_date_ca) as RN 
           from(select subj_id as subj, to_date(drawn_date,'DD.MM.YY') as act_date_RODS, case when bus_prod_sub_tp_id='ROD_STANDARD' and drawn_flag=1 then 1 else 0 end as OWNR_RODS_FLAG
           from adsl2_crm_owner.cmt_cl_appl where  bus_prod_sub_tp_id='ROD_STANDARD' and drawn_flag=1 and upd_dt>to_date(sysdate-3) order by drawn_date desc)a
           left join (select subj_id, to_date(act_date,'DD.MM.YY') as act_date_ca from l3_ekg_mikro)b on b.subj_id = a.subj)
           WHERE ACT_DATE_RODS >= ACT_DATE_CA and RN=1)a)j on j.subj_id=c.subj_id           
left join (select a.*  from (select subj_id,act_date_CL,OWNR_CL_FLAG from(
           select a.*,b.*, ROW_Number () over (partition by subj,act_date_ca order by subj,act_date_ca) as RN 
           from(select subj_id as subj, to_date(drawn_date,'DD.MM.YY') as act_date_CL, case when bus_prod_sub_tp_id  in ('RCL_STANDARD','RCL_REFI','RCL_CONS') and drawn_flag=1 then 1 else 0 end as OWNR_CL_FLAG
           from adsl2_crm_owner.cmt_cl_appl where  bus_prod_sub_tp_id in ('RCL_STANDARD','RCL_REFI','RCL_CONS')and drawn_flag=1 and upd_dt>to_date(sysdate-3)order by drawn_date desc)a
           left join (select subj_id, to_date(act_date,'DD.MM.YY') as act_date_ca from l3_ekg_mikro)b on b.subj_id = a.subj)
           WHERE ACT_DATE_CL >= ACT_DATE_CA and RN=1)a)k on k.subj_id=c.subj_id
left join (select 
cif,count(*), 
round(AVG(principal),0) AS Lifetime_AvgBal
from (
select 
a.cif,
a.subj_id,
a.acc_key, 
b.acc_class_id,
b.ccy,
c.principal,
c.snap_date
from 
         (SELECT CMT.cif,cmt.subj_id, B.acc_key 
         FROM ADSL2_CRM_OWNER.CMT_SUBJ CMT 
         LEFT JOIN ADSL2_CRM_OWNER.CMT_DEPO_APPL B ON B.SUBJ_ID=CMT.SUBJ_ID AND B.DEL_FLAG<>1
         WHERE CMT.DEL_FLAG<>1 and B.upd_dt>to_date(sysdate-3))a
left join (SELECT DISTINCT a.acc_key,a.ccy_descr ccy,a.acc_class_id
           FROM ADSl2_crm_owner.cmt_depo_appl a
           where acc_class_id='CA0001' and A.upd_dt>to_date(sysdate-3))b
on b.acc_key=a.acc_key
left join (select c.acc_key,a.snap_date,case when a.tot_princ<0 then 0 else tot_princ end as principal
           from adsl2_risk_owner.balance a
           left join adsl2_crm_owner.cmt_depo_appl c on c.acc_key=a.acc_key and c.del_flag<>1
           where c.subj_id is not null and a.upd_dt>to_date(sysdate-3))c
on c.acc_key=a.acc_key)
GROUP BY cif)o  
on o.cif=c.cif)) A)A)A)

) 
nd on (l3.cif = nd.cif)


WHEN MATCHED THEN
     UPDATE SET 

l3.RN=nd.RN,
l3.SUBJ_ID=nd.SUBJ_ID,
l3.ACC_NUM=nd.ACC_NUM,
l3.AGE=nd.AGE,  
l3.GENDER_FLAG=nd.GENDER_FLAG,
l3.FIRST_TOUCH_POINT=nd.FIRST_TOUCH_POINT,
l3.FULFILLMENT_CNL=nd.FULFILLMENT_CNL,
l3.PROMOTIONAL_CODE=nd.PROMOTIONAL_CODE,
l3.SUM_IBS_LOGIN=nd.SUM_IBS_LOGIN,
l3.SUM_MEQUA_LOGIN=nd.SUM_MEQUA_LOGIN,
l3.FIRST_MONTH_BAL=nd.FIRST_MONTH_BAL,
l3.FIRST_3_MONTHS_BAL=nd.FIRST_3_MONTHS_BAL,
l3.LIFETIME_AVGBAL=nd.LIFETIME_AVGBAL,
l3.LIFETIME_ATM_AMT=nd.LIFETIME_ATM_AMT,
l3.LIFETIME_POS_AMT=nd.LIFETIME_POS_AMT,
l3.OWNR_CLOSE_FLAG=nd.OWNR_CLOSE_FLAG,
l3.OWNR_OPEN_FLAG=nd.OWNR_OPEN_FLAG,
l3.OWNR_FROZEN_FLAG=nd.OWNR_FROZEN_FLAG,
l3.APPL_CL_FLAG=nd.APPL_CL_FLAG,
l3.OWNR_CL_FLAG=nd.OWNR_CL_FLAG,
l3.OWNR_RODS_FLAG=nd.OWNR_RODS_FLAG,
l3.APPL_SA_FLAG=nd.APPL_SA_FLAG,
l3.OWNR_SA_FLAG=nd.OWNR_SA_FLAG,
l3.APPL_TD_FLAG=nd.APPL_TD_FLAG,
l3.OWNR_TD_FLAG=nd.OWNR_TD_FLAG,
l3.APPL_MRG_FLAG=nd.APPL_MRG_FLAG,
l3.OWNR_MRG_FLAG=nd.OWNR_MRG_FLAG,
l3.OWNR_INST_FLAG=nd.OWNR_INST_FLAG,
l3.APPL_DATE=nd.APPL_DATE,
l3.ACT_DATE=nd.ACT_DATE,
l3.CLOSE_DATE=nd.CLOSE_DATE,
l3.ACT_SUBJ_FLAG=nd.ACT_SUBJ_FLAG,
l3.DIFF_APPL_ACT_DAYS=nd.DIFF_APPL_ACT_DAYS,
l3.DIFF_APPL_ACT_MONTHS=nd.DIFF_APPL_ACT_MONTHS,
l3.DIFF_APPL_ACT_YEARS=nd.DIFF_APPL_ACT_YEARS,
l3.DIFF_APPL_CLOSE_DAYS=nd.DIFF_APPL_CLOSE_DAYS,
l3.DIFF_APPL_CLOSE_MONTHS=nd.DIFF_APPL_CLOSE_MONTHS,
l3.DIFF_APPL_CLOSE_YEARS=nd.DIFF_APPL_CLOSE_YEARS,
l3.DIFF_ACT_CLOSE_DAYS=nd.DIFF_ACT_CLOSE_DAYS,
l3.DIFF_ACT_CLOSE_MONTHS=nd.DIFF_ACT_CLOSE_MONTHS,
l3.DIFF_ACT_CLOSE_YEARS=nd.DIFF_ACT_CLOSE_YEARS,
l3.DIFF_APPL_SYS_DAYS=nd.DIFF_APPL_SYS_DAYS,
l3.DIFF_APPL_SYS_MONTHS=nd.DIFF_APPL_SYS_MONTHS,
l3.DIFF_APPL_SYS_YEARS=nd.DIFF_APPL_SYS_YEARS,
l3.DIFF_ACT_SYS_DAYS=nd.DIFF_ACT_SYS_DAYS,
l3.DIFF_ACT_SYS_MONTHS=nd.DIFF_ACT_SYS_MONTHS,
l3.DIFF_ACT_SYS_YEARS=nd.DIFF_ACT_SYS_YEARS,

l3.ACT_DATE_CL=nd.ACT_DATE_CL,
l3.DIFF_ACT_CL_DAYS=nd.DIFF_ACT_CL_DAYS,
l3.DIFF_ACT_CL_MONTHS=nd.DIFF_ACT_CL_MONTHS,
l3.DIFF_ACT_CL_YEARS=nd.DIFF_ACT_CL_YEARS,
l3.DIFF_ACT_SYS_CL_DAYS=nd.DIFF_ACT_SYS_CL_DAYS,
l3.DIFF_ACT_SYS_CL_MONTHS=nd.DIFF_ACT_SYS_CL_MONTHS,
l3.DIFF_ACT_SYS_CL_YEARS=nd.DIFF_ACT_SYS_CL_YEARS,

l3.ACT_DATE_RODS=nd.ACT_DATE_RODS,
l3.DIFF_ACT_RODS_DAYS=nd.DIFF_ACT_RODS_DAYS,
l3.DIFF_ACT_RODS_MONTHS=nd.DIFF_ACT_RODS_MONTHS,
l3.DIFF_ACT_RODS_YEARS=nd.DIFF_ACT_RODS_YEARS,
l3.DIFF_ACT_SYS_RODS_DAYS=nd.DIFF_ACT_SYS_RODS_DAYS,
l3.DIFF_ACT_SYS_RODS_MONTHS=nd.DIFF_ACT_SYS_RODS_MONTHS,
l3.DIFF_ACT_SYS_RODS_YEARS=nd.DIFF_ACT_SYS_RODS_YEARS,

l3.ACT_DATE_SA=nd.ACT_DATE_SA,
l3.DIFF_ACT_SA_DAYS=nd.DIFF_ACT_SA_DAYS,
l3.DIFF_ACT_SA_MONTHS=nd.DIFF_ACT_SA_MONTHS,
l3.DIFF_ACT_SA_YEARS=nd.DIFF_ACT_SA_YEARS,
l3.DIFF_ACT_SYS_SA_DAYS=nd.DIFF_ACT_SYS_SA_DAYS,
l3.DIFF_ACT_SYS_SA_MONTHS=nd.DIFF_ACT_SYS_SA_MONTHS,
l3.DIFF_ACT_SYS_SA_YEARS=nd.DIFF_ACT_SYS_SA_YEARS,

l3.ACT_DATE_TD=nd.ACT_DATE_TD,
l3.DIFF_ACT_TD_DAYS=nd.DIFF_ACT_TD_DAYS,
l3.DIFF_ACT_TD_MONTHS=nd.DIFF_ACT_TD_MONTHS,
l3.DIFF_ACT_TD_YEARS=nd.DIFF_ACT_TD_YEARS,
l3.DIFF_ACT_SYS_TD_DAYS=nd.DIFF_ACT_SYS_TD_DAYS,
l3.DIFF_ACT_SYS_TD_MONTHS=nd.DIFF_ACT_SYS_TD_MONTHS,
l3.DIFF_ACT_SYS_TD_YEARS=nd.DIFF_ACT_SYS_TD_YEARS,

l3.ACT_DATE_MRG=nd.ACT_DATE_MRG,
l3.DIFF_ACT_MRG_DAYS=nd.DIFF_ACT_MRG_DAYS,
l3.DIFF_ACT_MRG_MONTHS=nd.DIFF_ACT_MRG_MONTHS,
l3.DIFF_ACT_MRG_YEARS=nd.DIFF_ACT_MRG_YEARS,
l3.DIFF_ACT_SYS_MRG_DAYS=nd.DIFF_ACT_SYS_MRG_DAYS,
l3.DIFF_ACT_SYS_MRG_MONTHS=nd.DIFF_ACT_SYS_MRG_MONTHS,
l3.DIFF_ACT_SYS_MRG_YEARS=nd.DIFF_ACT_SYS_MRG_YEARS,

l3.ACT_DATE_INST=nd.ACT_DATE_INST,
l3.DIFF_ACT_INST_DAYS=nd.DIFF_ACT_INST_DAYS,
l3.DIFF_ACT_INST_MONTHS=nd.DIFF_ACT_INST_MONTHS,
l3.DIFF_ACT_INST_YEARS=nd.DIFF_ACT_INST_YEARS,
l3.DIFF_ACT_SYS_INST_DAYS=nd.DIFF_ACT_SYS_INST_DAYS,
l3.DIFF_ACT_SYS_INST_MONTHS=nd.DIFF_ACT_SYS_INST_MONTHS,
l3.DIFF_ACT_SYS_INST_YEARS=nd.DIFF_ACT_SYS_INST_YEARS,


l3.MIN_D=nd.MIN_D,
l3.MIN_M=nd.MIN_M,
l3.MIN_Y=nd.MIN_Y,
l3.NAME_D=nd.NAME_D,
l3.NAME_M=nd.NAME_M,
l3.NAME_Y=nd.NAME_Y,
l3.PRE_APPR_OFR_FLAG=nd.PRE_APPR_OFR_FLAG,
l3.MRKT_CNST_FLAG=nd.MRKT_CNST_FLAG


WHEN NOT MATCHED THEN 
     INSERT (
RN,
SUBJ_ID,
ACC_NUM,
AGE,  
GENDER_FLAG,
FIRST_TOUCH_POINT,
FULFILLMENT_CNL,
PROMOTIONAL_CODE,
SUM_IBS_LOGIN,
SUM_MEQUA_LOGIN,
FIRST_MONTH_BAL,
FIRST_3_MONTHS_BAL,
LIFETIME_AVGBAL,
LIFETIME_ATM_AMT,
LIFETIME_POS_AMT,
OWNR_CLOSE_FLAG,
OWNR_OPEN_FLAG,
OWNR_FROZEN_FLAG,
APPL_CL_FLAG,
OWNR_CL_FLAG,
OWNR_RODS_FLAG,
APPL_SA_FLAG,
OWNR_SA_FLAG,
APPL_TD_FLAG,
OWNR_TD_FLAG,
APPL_MRG_FLAG,
OWNR_MRG_FLAG,
OWNR_INST_FLAG,
APPL_DATE,
ACT_DATE,
CLOSE_DATE,
ACT_SUBJ_FLAG,
DIFF_APPL_ACT_DAYS,
DIFF_APPL_ACT_MONTHS,
DIFF_APPL_ACT_YEARS,
DIFF_APPL_CLOSE_DAYS,
DIFF_APPL_CLOSE_MONTHS,
DIFF_APPL_CLOSE_YEARS,
DIFF_ACT_CLOSE_DAYS,
DIFF_ACT_CLOSE_MONTHS,
DIFF_ACT_CLOSE_YEARS,
DIFF_APPL_SYS_DAYS,
DIFF_APPL_SYS_MONTHS,
DIFF_APPL_SYS_YEARS,
DIFF_ACT_SYS_DAYS,
DIFF_ACT_SYS_MONTHS,
DIFF_ACT_SYS_YEARS,

ACT_DATE_CL,
DIFF_ACT_CL_DAYS,
DIFF_ACT_CL_MONTHS,
DIFF_ACT_CL_YEARS,

DIFF_ACT_SYS_CL_DAYS,
DIFF_ACT_SYS_CL_MONTHS,
DIFF_ACT_SYS_CL_YEARS,

ACT_DATE_RODS,
DIFF_ACT_RODS_DAYS,
DIFF_ACT_RODS_MONTHS,
DIFF_ACT_RODS_YEARS,

DIFF_ACT_SYS_RODS_DAYS,
DIFF_ACT_SYS_RODS_MONTHS,
DIFF_ACT_SYS_RODS_YEARS,

ACT_DATE_SA,
DIFF_ACT_SA_DAYS,
DIFF_ACT_SA_MONTHS,
DIFF_ACT_SA_YEARS,

DIFF_ACT_SYS_SA_DAYS,
DIFF_ACT_SYS_SA_MONTHS,
DIFF_ACT_SYS_SA_YEARS,

ACT_DATE_TD,
DIFF_ACT_TD_DAYS,
DIFF_ACT_TD_MONTHS,
DIFF_ACT_TD_YEARS,

DIFF_ACT_SYS_TD_DAYS,
DIFF_ACT_SYS_TD_MONTHS,
DIFF_ACT_SYS_TD_YEARS,

ACT_DATE_MRG,
DIFF_ACT_MRG_DAYS,
DIFF_ACT_MRG_MONTHS,
DIFF_ACT_MRG_YEARS,

DIFF_ACT_SYS_MRG_DAYS,
DIFF_ACT_SYS_MRG_MONTHS,
DIFF_ACT_SYS_MRG_YEARS,

ACT_DATE_INST,
DIFF_ACT_INST_DAYS,
DIFF_ACT_INST_MONTHS,
DIFF_ACT_INST_YEARS,

DIFF_ACT_SYS_INST_DAYS,
DIFF_ACT_SYS_INST_MONTHS,
DIFF_ACT_SYS_INST_YEARS,

MIN_D,
MIN_M,
MIN_Y,
NAME_D,
NAME_M,
NAME_Y,
PRE_APPR_OFR_FLAG,
MRKT_CNST_FLAG

) 
     VALUES (
nd.RN,
nd.SUBJ_ID,
nd.ACC_NUM,
nd.AGE,  
nd.GENDER_FLAG,
nd.FIRST_TOUCH_POINT,
nd.FULFILLMENT_CNL,
nd.PROMOTIONAL_CODE,
nd.SUM_IBS_LOGIN,
nd.SUM_MEQUA_LOGIN,
nd.FIRST_MONTH_BAL,
nd.FIRST_3_MONTHS_BAL,
nd.LIFETIME_AVGBAL,
nd.LIFETIME_ATM_AMT,
nd.LIFETIME_POS_AMT,
nd.OWNR_CLOSE_FLAG,
nd.OWNR_OPEN_FLAG,
nd.OWNR_FROZEN_FLAG,
nd.APPL_CL_FLAG,
nd.OWNR_CL_FLAG,
nd.OWNR_RODS_FLAG,
nd.APPL_SA_FLAG,
nd.OWNR_SA_FLAG,
nd.APPL_TD_FLAG,
nd.OWNR_TD_FLAG,
nd.APPL_MRG_FLAG,
nd.OWNR_MRG_FLAG,
nd.OWNR_INST_FLAG,
nd.APPL_DATE,
nd.ACT_DATE,
nd.CLOSE_DATE,
nd.ACT_SUBJ_FLAG,
nd.DIFF_APPL_ACT_DAYS,
nd.DIFF_APPL_ACT_MONTHS,
nd.DIFF_APPL_ACT_YEARS,
nd.DIFF_APPL_CLOSE_DAYS,
nd.DIFF_APPL_CLOSE_MONTHS,
nd.DIFF_APPL_CLOSE_YEARS,
nd.DIFF_ACT_CLOSE_DAYS,
nd.DIFF_ACT_CLOSE_MONTHS,
nd.DIFF_ACT_CLOSE_YEARS,
nd.DIFF_APPL_SYS_DAYS,
nd.DIFF_APPL_SYS_MONTHS,
nd.DIFF_APPL_SYS_YEARS,
nd.DIFF_ACT_SYS_DAYS,
nd.DIFF_ACT_SYS_MONTHS,
nd.DIFF_ACT_SYS_YEARS,

nd.ACT_DATE_CL,
nd.DIFF_ACT_CL_DAYS,
nd.DIFF_ACT_CL_MONTHS,
nd.DIFF_ACT_CL_YEARS,

nd.DIFF_ACT_SYS_CL_DAYS,
nd.DIFF_ACT_SYS_CL_MONTHS,
nd.DIFF_ACT_SYS_CL_YEARS,

nd.ACT_DATE_RODS,
nd.DIFF_ACT_RODS_DAYS,
nd.DIFF_ACT_RODS_MONTHS,
nd.DIFF_ACT_RODS_YEARS,

nd.DIFF_ACT_SYS_RODS_DAYS,
nd.DIFF_ACT_SYS_RODS_MONTHS,
nd.DIFF_ACT_SYS_RODS_YEARS,

nd.ACT_DATE_SA,
nd.DIFF_ACT_SA_DAYS,
nd.DIFF_ACT_SA_MONTHS,
nd.DIFF_ACT_SA_YEARS,

nd.DIFF_ACT_SYS_SA_DAYS,
nd.DIFF_ACT_SYS_SA_MONTHS,
nd.DIFF_ACT_SYS_SA_YEARS,

nd.ACT_DATE_TD,
nd.DIFF_ACT_TD_DAYS,
nd.DIFF_ACT_TD_MONTHS,
nd.DIFF_ACT_TD_YEARS,

nd.DIFF_ACT_SYS_TD_DAYS,
nd.DIFF_ACT_SYS_TD_MONTHS,
nd.DIFF_ACT_SYS_TD_YEARS,

nd.ACT_DATE_MRG,
nd.DIFF_ACT_MRG_DAYS,
nd.DIFF_ACT_MRG_MONTHS,
nd.DIFF_ACT_MRG_YEARS,

nd.DIFF_ACT_SYS_MRG_DAYS,
nd.DIFF_ACT_SYS_MRG_MONTHS,
nd.DIFF_ACT_SYS_MRG_YEARS,

nd.ACT_DATE_INST,
nd.DIFF_ACT_INST_DAYS,
nd.DIFF_ACT_INST_MONTHS,
nd.DIFF_ACT_INST_YEARS,

nd.DIFF_ACT_SYS_INST_DAYS,
nd.DIFF_ACT_SYS_INST_MONTHS,
nd.DIFF_ACT_SYS_INST_YEARS,

nd.MIN_D,
nd.MIN_M,
nd.MIN_Y,
nd.NAME_D,
nd.NAME_M,
nd.NAME_Y,
nd.PRE_APPR_OFR_FLAG,
nd.MRKT_CNST_FLAG

);
COMMIT;